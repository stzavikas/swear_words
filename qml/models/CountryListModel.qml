import VPlayApps 1.0
import QtQuick 2.0


ListModel {
    id: countryModel
    ListElement {
        latitude: 47.516231
        longitude: 14.550072
        country_name: "Austria"
        swear_list_file: "../../assets/swears/austria.json"
    }
    ListElement {
        latitude: 39.074208
        longitude: 21.824312
        country_name: "Greece"
        swear_list_file: "../../assets/swears/greece.json"
    }
    ListElement {
        latitude: 51.165691
        longitude: 10.451526
        country_name: "Germany"
        swear_list_file: "../../assets/swears/germany.json"
    }
    ListElement {
        latitude: 55.378051
        longitude: -3.435973
        country_name: "United Kingdom"
        swear_list_file: "../../assets/swears/unitedkingdom.json"
    }
}

