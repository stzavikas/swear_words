import VPlayApps 1.0
import QtQuick 2.0

import "pages"



Page {
  // make page navigation public, so app-demo launcher can track navigation changes and log screens with Google Analytics
  property alias navigation: navigation

  // for windows platform check
  property bool isWindows: system.isPlatform(4) || system.isPlatform(9) || system.isPlatform(10) // windows, winPhone or winRT

  Navigation {
    id: navigation

    NavigationItem {
      id: mapItem
      title: "Select on map"
      icon: IconType.bolt

      NavigationStack {
        MapSelectionPage { title: mapItem.title }
      }
    }

    NavigationItem {
      id: selectItem
      title: "Select from list"
      icon: IconType.bolt

      NavigationStack {
        ListSelectionPage { title: mapItem.title }
      }
    }

    NavigationItem {
      id: listViewItem
      title: "Result List"
      icon: IconType.list

      NavigationStack {
        ResultListPage { title: listViewItem.title }
      }
    }
  }
}

