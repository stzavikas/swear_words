import VPlayApps 1.0
import QtQuick 2.0
import QtPositioning 5.5
import QtLocation 5.5

import "../models"
import "../pages"


Page {
    id: page
    property int selectedIndex: -1
    property string countryName: ""

    ListModel {
        id: swearListModel
    }

    function onSelectedIndexChanged() {
        console.log("Country name: " + countryModel.get(page.selectedIndex).country_name)
        page.countryName = countryModel.get(page.selectedIndex).country_name
        swearListModel.clear();
        //TODO: Replace JS reading from file with C++
        var xhr = new XMLHttpRequest();
        xhr.open("GET", Qt.resolvedUrl(countryModel.get(page.selectedIndex).swear_list_file));
        xhr.onreadystatechange = function() {
            if ( xhr.readyState == xhr.DONE) {
                if ( xhr.status == 200) {
                    var jsonObject = JSON.parse(xhr.responseText);
                    loaded(jsonObject)
                }
                else {
                    console.log("No such file: " + countryModel.get(page.selectedIndex).swear_list_file);
                }
            }
        }
        xhr.send();
        function loaded(jsonObject) {
            swearListModel.clear()
            for ( var index in jsonObject.all ) {
                swearListModel.append({
                    word: jsonObject.all[index].swear["word"],
                    translation: jsonObject.all[index].swear["translation"]});
            }
            navigationStack.popAllExceptFirstAndPush(resultListComponent, {incomingModel: swearListModel})
        }
    }

    title: qsTr("Main Page")

    CountryListModel { }

    AppMap {
        id: countryMap
        anchors.fill: parent

        // configure plugin for displaying map here
        // see http://doc.qt.io/qt-5/qtlocation-index.html#plugin-references-and-parameters
        // for a documentation of possible Location Plugins
        plugin: Plugin {
          name: "mapbox"
          // configure your own map_id and access_token here
          parameters: [  PluginParameter {
              name: "mapbox.mapping.map_id"
              value: "mapbox.streets"
            },
            PluginParameter {
              name: "mapbox.access_token"
              value: "pk.eyJ1IjoiZ3R2cGxheSIsImEiOiJjaWZ0Y2pkM2cwMXZqdWVsenJhcGZ3ZDl5In0.6xMVtyc0CkYNYup76iMVNQ"
            },
            PluginParameter {
              name: "mapbox.mapping.highdpi_tiles"
              value: true
            }]
        }

        // Center map to Vienna, AT
        center: QtPositioning.coordinate(48.208417, 16.372472)
        zoomLevel: 4

        MapItemView {
          model: countryModel

          delegate: MapQuickItem {
            id: countryDelegate
            coordinate: QtPositioning.coordinate(latitude, longitude)

            anchorPoint.x: image.width * 0.5
            anchorPoint.y: image.height

            sourceItem: AppImage {
              id: image

              width: dp(20)
              height: dp(17)

              source: {
                  return "../../assets/pin-green.png"
              }

              MouseArea {
                anchors.fill: parent
                onClicked: {
                    page.selectedIndex = index
                    page.onSelectedIndexChanged()
                }
              }
            }
          }
        }
    }
}
